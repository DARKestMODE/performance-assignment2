package hw3

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"strconv"
	"strings"
)

/*
Important ones:
	1 - User struct with easyjson
	2 - strings.Contains()
	3 - scanner (bytes/op reduced)
Minor ones:
	1 - string.Builder()
	2 - hashMap
*/

// вам надо написать более быструю оптимальную этой функции
func FastSearch(out io.Writer) {
	//SlowSearch(out)
	file, err := os.Open(filePath)
	if err != nil {
		panic(err)
	}

	scanner := bufio.NewScanner(file)

	seenBrowsers := map[string]string{}
	b := strings.Builder{}
	i := 0
	isAndroid := false
	isMSIE := false
	user := &User{}

	for scanner.Scan() {
		err := user.UnmarshalJSON(scanner.Bytes())
		if err != nil {
			panic(err)
		}

		isAndroid = false
		isMSIE = false

		for _, browser := range user.Browsers {
			if strings.Contains(browser, "Android") {
				isAndroid = true
			} else if strings.Contains(browser, "MSIE") {
				isMSIE = true
			} else {
				continue
			}
			seenBrowsers[browser] = browser
		}

		if isAndroid && isMSIE {
			email := strings.Replace(user.Email, "@", " [at] ", 1)
			b.WriteString("[" +strconv.Itoa(i)+"] "+user.Name+" <"+email+">\n")
		}

		i++
	}

	fmt.Fprint(out, "found users:\n"+b.String()+"\n")
	fmt.Fprintln(out, "Total unique browsers", len(seenBrowsers))
}

//easyjson
type User struct {
	Browsers []string `json:"browsers"`
	Company  string   `json:"company"`
	Country  string   `json:"country"`
	Email    string   `json:"email"`
	Job      string   `json:"job"`
	Name     string   `json:"name"`
	Phone    string   `json:"phone"`
}
